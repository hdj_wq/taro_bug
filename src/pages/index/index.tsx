import { View, Text ,ScrollView} from '@tarojs/components'
import { useLoad } from '@tarojs/taro'
import './index.less'

export default function Index() {

  useLoad(() => {
    console.log('Page loaded.')
  })

  return (
    <ScrollView className='index'>
      {[1,2,3,4,5,6,7,8,9,10].map((item)=>{
        return  <View style={{height:200}} key={item}>
          <Text>{item}</Text>
        </View>
      })}
    </ScrollView>
  )
}
